import ComposableArchitecture
import Foundation

struct Storage {
    static let storageKey = "BRProject.history"
    static let storage = UserDefaults.standard

    var store: (_ data: [String]) -> Void
    var load: () -> [String]
}

extension Storage: DependencyKey {
    static var liveValue: Storage = .init { data in
        storage.set(data, forKey: storageKey)
    } load: {
        storage.stringArray(forKey: storageKey) ?? []
    }

    static var testValue: Storage {
        .init { _ in

        } load: {
            []
        }
    }
}

extension DependencyValues {
    var storage: Storage {
        get { self[Storage.self] }
        set { self[Storage.self] = newValue }
    }
}
