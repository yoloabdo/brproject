import Foundation

enum URLBuildError: Error {
    case badURL
}

enum SearchQueryURLBuilder {
    static let mainURL = "https://www.flickr.com/services/rest/"
    static let format = "format=json&nojsoncallback=1"

    static func search(query: String, page: Int) throws -> URLRequest {
        let perPage = 25
        let string = "\(mainURL)?method=\(FlickrMethod.search.rawValue)&api_key=\(FlickrKeys.key)&text=\(query)&&per_page=\(perPage)&page=\(page)&\(format)"

        guard let url = URL(string: string) else {
            throw URLBuildError.badURL
        }
        return URLRequest(url: url)
    }

    static func loadImage(id: String) throws -> URLRequest {
        let string = "\(mainURL)?method=\(FlickrMethod.sizes.rawValue)&api_key=\(FlickrKeys.key)&photo_id=\(id)&\(format)"
        guard let url = URL(string: string) else {
            throw URLBuildError.badURL
        }
        return URLRequest(url: url)
    }
}

enum FlickrMethod: String {
    case search = "flickr.photos.search"
    case sizes = "flickr.photos.getSizes"
}
