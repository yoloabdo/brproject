import ComposableArchitecture
import Foundation
import Nuke
import SwiftUI
import UIKit

struct SearchQuery: Equatable {
    var page: Int
    var text: String
}

struct NetworkClient {
    var load: (_ query: SearchQuery) async throws -> [ImageDetailsDTO]
}

extension NetworkClient: DependencyKey {
    static var liveValue: NetworkClient {
        .init { query in
            let data = try await NetworkService().fetch(type: FlickrData.self, with: SearchQueryURLBuilder.search(query: query.text, page: query.page))
            let photos = data.photos.photo
            var results: [ImageDetailsDTO] = []

            try await withThrowingTaskGroup(of: ImageDetailsDTO.self, body: { taskGroup in
                for photo in photos {
                    taskGroup.addTask {
                        let flickrphoto = try await NetworkService().fetch(
                            type: FlickrImageData.self,
                            with: SearchQueryURLBuilder.loadImage(id: photo.id)
                        )
                        return .init(id: photo.id, title: photo.title, data: flickrphoto)
                    }
                }

                for try await image in taskGroup {
                    results.append(image)
                }

            })

            return results
        }
    }

    static var testValue: NetworkClient {
        .init { _ in
            []
        }
    }

    static var errorValue: NetworkClient {
        .init { _ in
            throw URLError(.badURL)
        }
    }
}

extension DependencyValues {
    var networkClient: NetworkClient {
        get { self[NetworkClient.self] }
        set { self[NetworkClient.self] = newValue }
    }
}

struct ImageLoader {
    var loadImage: (_ url: URL) async throws -> UIImage
}

extension ImageLoader: DependencyKey {
    static var liveValue: ImageLoader {
        .init { url in
            let response = try await ImagePipeline.shared.image(for: url)
            return response.image
        }
    }
}

extension DependencyValues {
    var imageClient: ImageLoader {
        get { self[ImageLoader.self] }
        set { self[ImageLoader.self] = newValue }
    }
}
