import Foundation

// MARK: - FlickrImageData

struct FlickrImageData: Codable, Equatable {
    let sizes: Sizes
}

// MARK: - Sizes
struct Sizes: Codable, Equatable {
    let size: [Size?]
}

// MARK: - Size
struct Size: Codable, Equatable, Hashable {
    let label: String
    let source: String?
    let url: String?
}

enum ImageSize: String, Codable, Equatable {
    case small400 = "Small 400"
    case thumbnail = "Thumbnail"
    case original = "Original"
}
