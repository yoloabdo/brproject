import Foundation

struct ImageDetailsDTO: Identifiable, Equatable, Hashable {
    let id: String
    let title: String?
    let size: [Size]
}

extension ImageDetailsDTO {
    init(id: String, title: String?, data: FlickrImageData) {
        self.id = id
        self.title = title
        size = data.sizes.size.compactMap { $0 }
    }
}

extension ImageDetailsDTO {
    var thumbnail: Size? {
        // Small 400 is better than thumbnail for iOS devices
        size.first { item in
            item.label == "Small 400"
        }
    }

    var largeSize: Size? {
        // last one is always the biggest according to the API
        size.last
    }
}
