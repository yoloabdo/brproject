import Foundation

// MARK: - FlickrData

struct FlickrData: Codable {
    let photos: Photos
    let stat: String
}

// MARK: - Photos

struct Photos: Codable {
    let page: Int?
    let pages: Int?
    let perpage: Int?
    let total: Int?
    let photo: [Photo]
}

// MARK: - Photo

struct Photo: Codable, Equatable {
    let id: String
    let owner, secret, server: String?
    let farm: Int?
    let title: String?
    let ispublic, isfriend, isfamily: Int?
}
