import ComposableArchitecture
import SwiftUI
import SwiftUINavigation

struct HomeFeature: ReducerProtocol {
    struct State: Equatable {
        var searchQuery = ""
        var isLoading = false
        var searchQueries: Set<String> = .init()
        var queryRequest = SearchQuery(page: 1, text: "")
        var searchResults: IdentifiedArrayOf<ImageDetailsDTO> = []
        @BindableState var selectedPhoto: ImageDetailsDTO?
        var detailsStore: ImageDetailsFeature.State?
        var isNavigationActive = false

        var searchQueriesArray: [String] {
            Array(searchQueries)
        }

        var detailsStores: IdentifiedArrayOf<ImageThumbnailFeature.State> {
            get { .init(uniqueElements: searchResults.map { ImageThumbnailFeature.State($0) }) }
            set { _ = newValue }
        }

        var alert: AlertState<AlertAction>?
    }

    enum AlertAction: Equatable {
        case alertDismissed
    }

    enum Action: BindableAction, Equatable {
        case binding(BindingAction<State>)
        case searchBarChanged(text: String)
        case initiateSearch
        case photosResponse(TaskResult<[ImageDetailsDTO]>)
        case buildRequest
        case sendRequest
        case thumbnail(id: ImageThumbnailFeature.State.ID, action: ImageThumbnailFeature.Action)
        case loadMore
        case details(action: ImageDetailsFeature.Action)
        case saveHistory
        case loadHistory
        case alert(AlertAction)
    }

    @Dependency(\.networkClient) var network
    @Dependency(\.storage) var storage

    //MARK: Reducer
    var body: some ReducerProtocol<State, Action> {
        BindingReducer()
        Reduce { state, action in
            switch action {
            case let .searchBarChanged(text: text):
                state.searchQuery = text.trimmingCharacters(in: .whitespacesAndNewlines)
                state.searchResults.removeAll()
                return .none
            case .initiateSearch:
                state.searchQueries.insert(state.searchQuery)
                state.isLoading = true
                return .task { .buildRequest }
            case let .photosResponse(.success(photos)):
                state.isLoading = false
                state.searchResults.append(contentsOf: photos)
                return .none
            case .photosResponse(.failure):
                state.alert = errorAlert
                state.isLoading = false
                return .none
            case .buildRequest:
                var queryRequest = state.queryRequest
                if queryRequest.text != state.searchQuery {
                    queryRequest.text = state.searchQuery
                    queryRequest.page = 1
                    state.searchResults.removeAll()
                    state.detailsStores.removeAll()
                }
                state.queryRequest = queryRequest
                return .task { .sendRequest }
            case .sendRequest:
                return .task { [queryRequest = state.queryRequest] in
                    await .photosResponse(
                        TaskResult {
                            try await self.network.load(queryRequest)
                        }
                    )
                }
            case let .thumbnail(id: id, action: action):
                switch action {
                case .onTap:
                    guard let photo = state.searchResults[id: id],
                          let source = photo.largeSize?.source,
                          let url = URL(string: source)
                    else { return .none }
                    state.selectedPhoto = photo
                    state.detailsStore = .init(url: url, title: photo.title ?? "Untitled")
                    return .none
                case .onAppear:
                    guard id == state.searchResults.last?.id else { return .none }
                    return .init(value: .loadMore)
                }
            case .loadMore:
                if !state.isLoading {
                    state.isLoading = true
                    state.queryRequest.page += 1
                    return .task { .buildRequest }
                }
                return .none
            case .saveHistory:
                storage.store(state.searchQueriesArray)
                return .none
            case .loadHistory:
                state.searchQueries = .init(storage.load())
                return .none
            case .binding, .details:
                return .none
            case .alert(AlertAction.alertDismissed):
                state.alert = nil
                return .none
            }
        }
        .forEach(
            \.detailsStores,
            action: /Action.thumbnail(id:action:)
        ) {
            ImageThumbnailFeature()
        }
        .ifLet(\.detailsStore, action: /Action.details(action:)) {
            ImageDetailsFeature()
        }
    }

    var errorAlert: AlertState<AlertAction> {
        AlertState {
            TextState("We've encountered an error, let's try that again")
        } actions: {
            ButtonState(action: .alertDismissed) {
                TextState("No worries")
            }
        } message: {
            TextState(
                "Maybe try again."
            )
        }
    }
}

extension View {
    func hideKeyboard() {
        let resign = #selector(UIResponder.resignFirstResponder)
        UIApplication.shared.sendAction(resign, to: nil, from: nil, for: nil)
    }
}

//MARK: - View
struct HomeView: View {
    var store: StoreOf<HomeFeature>
    var body: some View {
        WithViewStore(store, observe: { $0 }) { viewStore in
            NavigationStack {
                ZStack {
                    ThumbnailListView(store: store)
                    if viewStore.state.isLoading {
                        ProgressView()
                    } else if viewStore.detailsStores.isEmpty {
                        Text("Welcome to Flickr app")
                    }
                }
                .onAppear {
                    viewStore.send(.loadHistory)
                }
                .onDisappear {
                    viewStore.send(.saveHistory)
                }
                .navigationTitle("Flickr")
                .navigationDestination(unwrapping: viewStore.binding(\.$selectedPhoto)) { _ in
                    IfLetStore(store.scope(state: \.detailsStore, action: HomeFeature.Action.details(action:))) { store in
                        ImageDetailsView(store: store)
                    }
                }
                .alert(
                    self.store.scope(state: \.alert, action: HomeFeature.Action.alert),
                    dismiss: .alertDismissed
                )
            }
        }
    }
}

//MARK: - List view
struct ThumbnailListView: View {
    var store: StoreOf<HomeFeature>

    var body: some View {
        WithViewStore(store) { viewStore in
            List {
                ForEachStore(self.store.scope(
                    state: \.detailsStores,
                    action: HomeFeature.Action.thumbnail(id: action:)
                )
                ) { store in
                    ImageThumbnailView(store: store)
                        .listRowSeparator(.hidden)
                }
            }
            .listStyle(.inset)
            .searchable(
                text: viewStore.binding(
                    get: \.searchQuery,
                    send: HomeFeature.Action.searchBarChanged
                )
            )
            .onSubmit(of: .search) {
                viewStore.send(.initiateSearch)
            }
            .searchSuggestions {
                ForEach(viewStore.state.searchQueriesArray, id: \.self) { item in
                    Text(item)
                        .searchCompletion(item)
                        .task {
                            hideKeyboard()
                        }
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static let store: StoreOf<HomeFeature> = .init(
        initialState: .init(),
        reducer: HomeFeature()
    )
    static var previews: some View {
        HomeView(store: store)
    }
}
