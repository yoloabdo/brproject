import ComposableArchitecture
import Nuke
import NukeUI
import SwiftUI

struct ImageThumbnailFeature: ReducerProtocol {
    struct State: Identifiable, Equatable {
        var id: String
        var title: String
        var url: URL?
        var isLast = false

        init(_ imageDetails: ImageDetailsDTO) {
            id = imageDetails.id
            title = imageDetails.title ?? "Untitled"
            guard
                let source = imageDetails.thumbnail?.source,
                let url = URL(string: source)
            else {
                return
            }
            self.url = url
        }
    }

    enum Action: Equatable {
        case onTap
        case onAppear
    }

    func reduce(into _: inout State, action _: Action) -> EffectTask<Action> {
        return .none
    }
}

struct ImageThumbnailView: View {
    let store: StoreOf<ImageThumbnailFeature>

    var body: some View {
        WithViewStore(store, observe: { $0 }) { viewStore in
            ZStack(alignment: .bottom) {
                LazyImage(url: viewStore.state.url) { state in
                    if let image = state.image {
                        image
                    } else if state.error != nil {
                        Image(systemName: "photo.artframe")
                    }
                }
                .frame(height: 250)
                .clipped()
                ZStack(alignment: .leading) {
                    Color.black.opacity(0.7)
                        .frame(maxHeight: 25)
                    Text(viewStore.title)
                        .font(.footnote)
                        .multilineTextAlignment(.leading)
                        .lineLimit(1)
                        .fontWeight(.bold)
                        .foregroundColor(.white)
                        .padding(5)
                }
            }
            .contentShape(Rectangle())
            .onTapGesture {
                viewStore.send(.onTap)
            }
            .onAppear {
                viewStore.send(.onAppear)
            }
        }
    }
}
