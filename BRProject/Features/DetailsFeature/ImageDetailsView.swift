import ComposableArchitecture
import Nuke
import NukeUI
import SwiftUI

struct ImageDetailsFeature: ReducerProtocol {
    struct State: Equatable {
        let url: URL?
        var image: UIImage = .init(systemName: "photo.artframe")!
        var title: String = "Untitled"
    }

    enum Action: Equatable {
        case onAppear
        case photosResponse(TaskResult<UIImage>)
    }

    @Dependency(\.imageClient) var imageClient

    func reduce(into state: inout State, action: Action) -> EffectTask<Action> {
        switch action {
        case .onAppear:
            return .task { [url = state.url] in
                await .photosResponse(
                    TaskResult {
                        try await self.imageClient.loadImage(url!)
                    })
            }
        case let .photosResponse(.success(value)):
            state.image = value
            return .none
        case .photosResponse(.failure):
            return .none
        }
    }
}

struct ImageDetailsView: View {
    let store: StoreOf<ImageDetailsFeature>

    var body: some View {
        NavigationStack {
            WithViewStore(store) { viewStore in
                VStack {
                    Spacer()
                    PhotoView(image: viewStore.state.image)
                        .background {
                            Color.white
                        }
                        .navigationTitle(viewStore.state.title)
                }
                .onAppear {
                    viewStore.send(.onAppear)
                }
            }
        }
    }
}

struct ImageDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        ImageDetailsView(
            store: .init(
                initialState: .init(
                    url: URL(string: ""),
                    image: UIImage(systemName: "photo.artframe")!
                ),
                reducer: ImageDetailsFeature()
            )
        )
    }
}
