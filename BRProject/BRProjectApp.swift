import ComposableArchitecture
import SwiftUI
@main
struct BRProjectApp: App {
    let store: StoreOf<HomeFeature> = .init(
        initialState: .init(),
        reducer: HomeFeature()
    )

    var body: some Scene {
        WindowGroup {
            HomeView(store: store)
        }
    }
}
