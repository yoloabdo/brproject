import ComposableArchitecture
import XCTest

@testable import BRProject

@MainActor
final class HomeFeatureTests: XCTestCase {
    func test_initial_load_start_search_with_keyword() async throws {
        let store = TestStore(
            initialState: .init(),
            reducer: HomeFeature(network: .init(\.networkClient), storage: .init(\.storage))
        )

        let searchResults: [ImageDetailsDTO] = [.init(id: "123", title: "Image", data: .init(sizes: .init(size: [])))]
        store.dependencies.networkClient.load = { _ in searchResults }
        let searchHistory = ["test", "history"]
        store.dependencies.storage.load = { searchHistory }

        await store.send(.loadHistory) {
            $0.searchQueries = .init(searchHistory)
        }

        await store.send(.searchBarChanged(text: "cat")) {
            $0.searchQuery = "cat"
        }

        await store.send(.initiateSearch) {
            $0.isLoading = true
            $0.searchQueries.insert("cat")
        }

        await store.receive(.buildRequest) {
            $0.searchQueries.insert("cat")
            $0.queryRequest = .init(page: 1, text: "cat")
        }
        await store.receive(.sendRequest)
        await store.receive(.photosResponse(.success(searchResults))) {
            $0.isLoading = false
            $0.searchResults = .init(uniqueElements: searchResults)
        }
    }

    enum TestError: Error, Equatable {
        case load
    }

    func test_initial_load_start_search_with_keyword_failing_withAlertState() async throws {
        let store = TestStore(
            initialState: .init(),
            reducer: HomeFeature(network: .init(\.networkClient), storage: .init(\.storage))
        )

        store.dependencies.networkClient.load = { _ in throw TestError.load }
        let searchHistory = ["test", "history"]
        store.dependencies.storage.load = { searchHistory }

        await store.send(.loadHistory) {
            $0.searchQueries = .init(searchHistory)
        }

        await store.send(.searchBarChanged(text: "cat")) {
            $0.searchQuery = "cat"
        }

        await store.send(.initiateSearch) {
            $0.isLoading = true
            $0.searchQueries.insert("cat")
        }

        await store.receive(.buildRequest) {
            $0.searchQueries.insert("cat")
            $0.queryRequest = .init(page: 1, text: "cat")
        }
        await store.receive(.sendRequest)
        await store.receive(.photosResponse(.failure(TestError.load))) {
            $0.isLoading = false
            $0.alert = HomeFeature().errorAlert
        }
    }
}
